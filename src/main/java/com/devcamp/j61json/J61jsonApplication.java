package com.devcamp.j61json;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J61jsonApplication {

	public static void main(String[] args) {
		SpringApplication.run(J61jsonApplication.class, args);
	}

}
