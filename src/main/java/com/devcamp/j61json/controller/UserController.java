package com.devcamp.j61json.controller;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j61json.model.Item;
import com.devcamp.j61json.model.User;

@RestController
@CrossOrigin
public class UserController {

    @GetMapping("/user")
	public User getUser() {
		User user = new User(1, "John");
		Item item1 = new Item(2, "book", user);
		Item item2 = new Item(3, "livre", user);

		user.addItem(item1);
		user.addItem(item2);

		return user;
	}
	@GetMapping("/item")
	public ResponseEntity<Item> getItem() {
		User user = new User(1, "John");
		Item item = new Item(2, "book", user);
		Item item2 = new Item(3, "book", user);
		user.addItem(item);
		user.addItem(item2);
		return new ResponseEntity<>(item, HttpStatus.OK);
	}

	@GetMapping("/items")
	public ArrayList<Item> getItem2() {
		ArrayList<Item> listItem = new ArrayList<>();
		User user = new User(1, "John");
		Item item1 = new Item(2, "book", user);
		Item item2 = new Item(3, "livre", user);
		user.addItem(item1);
		user.addItem(item2);
		listItem.add(item1);
		listItem.add(item2);
		return listItem;
	}	
}

